﻿CREATE TABLE Employees (
	Name TEXT PRIMARY KEY,
	Salary INTEGER,
	Supervisor TEXT REFERENCES Employees
);

SELECT addValidTime('Employees');
SELECT addValidTimeReference('employees', 'employees', '{supervisor}');
SELECT addTransactionTime('Employees');

CREATE TABLE Teams (
	Name TEXT PRIMARY KEY
);
CREATE TABLE TimeOffs (
	Employee TEXT,
	TimeOffDate DATE,
	Comment TEXT,
	Hours INTEGER
);
SELECT addTransactionTime('timeoffs');
CREATE TABLE TeamEmployeeMap (
	Team TEXT REFERENCES Teams,
	Employee TEXT
);
SELECT addValidTime('TeamEmployeeMap');
SELECT addValidTimeReference('teamemployeemap', 'employees', '{employee}');
INSERT INTO Employees VALUES ('John Doe', 10000, NULL, PERIOD('1-1-2015', 'infinity'::TIMESTAMP));
INSERT INTO Employees VALUES ('John Minion', 10000, 'John Doe', PERIOD('1-1-2016', 'infinity'::TIMESTAMP));
-- Prikaz ktory porusuje referencnu integritu
--INSERT INTO Employees VALUES ('John Minion2', 10000, 'John Doe', PERIOD('1-1-2014', 'infinity'::TIMESTAMP));
INSERT INTO Employees VALUES ('John Doe', 20000, NULL, PERIOD('1-2-2016', 'infinity'::TIMESTAMP));
INSERT INTO Employees VALUES ('John Doe2', 23400, 'John Doe', PERIOD('1-2-2015', 'infinity'::TIMESTAMP));
INSERT INTO Teams VALUES ('A Team');
INSERT INTO Teams VALUES ('The Avengers');
INSERT INTO TeamEmployeeMap VALUES ('A Team', 'John Doe', PERIOD('5-1-2015', '1-4-2016'));
-- Prikaz ktory porusuje referencnu integritu
--INSERT INTO TeamEmployeeMap VALUES ('The Avengers', 'John Doe', PERIOD('1-1-2009', '1-4-2016'));

INSERT INTO Employees VALUES ('John Expensive', 50000, 'John Doe', PERIOD('1-3-2016', 'infinity'::TIMESTAMP));

INSERT INTO TimeOffs VALUES ('John Doe', '2016-01-05'::DATE, 'Dovolenka', 8);
INSERT INTO TimeOffs VALUES ('John Expensive', '2016-02-01'::DATE, 'Dovolenka', 8);
UPDATE TimeOffs SET Comment = 'Choroba' WHERE Employee = 'John Doe' AND TimeOffDate = '2016-01-05'::DATE;


-- Dovolenky v systeme k 1.1.2016
SELECT Employee, TimeOffDate, Hours FROM TimeOffsHistorical WHERE contains(transaction_time, '2016-01-01'::DATE);
-- Zamestnanci ktori posobili ako manazeri k 1.1.2016
SELECT DISTINCT Supervisor FROM Employees WHERE contains(valid_time, '2016-01-01'::DATE) AND Supervisor IS NOT NULL;
-- Zamestnanci ktorych sme najali pred zaciatkom 2016 ale nastupili az pocas tohoto roka
SELECT DISTINCT Name FROM Employees WHERE first(transaction_time) < '2016-01-01'::DATE AND first(valid_time) > '2016-01-01'::DATE;
-- Zamestnanci ktori nastupili bez zaradenia do tymu
SELECT DISTINCT Name FROM 
(SELECT Name, 	UNNEST(SEQUENCED(valid_time)) valid_time FROM Employees GROUP BY Name) e JOIN
(SELECT Employee, UNNEST(SEQUENCED(valid_time)) valid_time FROM TeamEmployeeMap GROUP BY Employee) tem
 ON e.Name = tem.Employee WHERE first(e.valid_time) < first(tem.valid_time)

-- Vypis obsahu tabulky zamestnancov
SELECT * FROM Employees;
