﻿-- @Start helper functions
-- Simple sorting function for arrays of periods
CREATE OR REPLACE FUNCTION sort(anyarray)
RETURNS anyarray AS $$
	select array(select $1[i] from generate_series(array_lower($1,1),
	array_upper($1,1)) g(i) order by 1)
$$ LANGUAGE SQL strict immutable;

-- Function to check if all periods in first list are contained by periods in 2nd list
-- This function relies on these lists being produced by SEQUENCED aggregate function
CREATE OR REPLACE FUNCTION periods_covered_by(referencing PERIOD[], referenced PERIOD[]) RETURNS boolean AS
$BODY$
DECLARE
	found BOOLEAN;
	i PERIOD;
	j PERIOD;
BEGIN
	FOREACH i IN ARRAY referencing LOOP
		found := false;
		FOREACH j IN ARRAY referenced LOOP
			IF contained_by(i, j) THEN
				found := true;
			END IF;
		END LOOP;
		IF NOT found THEN
			RETURN false;
		END IF;
	END LOOP;
	RETURN true;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Function to join list of periods as much as possible
CREATE OR REPLACE FUNCTION restructuralize_periods(periods PERIOD[]) RETURNS PERIOD[] AS
$BODY$
DECLARE
	ret PERIOD[] := '{}';
	pds PERIOD[];
	tmp PERIOD := NULL;
	i PERIOD;
BEGIN
	pds := sort(periods);
	FOREACH i IN ARRAY pds LOOP
		IF tmp IS NULL THEN
			tmp := i;
		ELSE
			IF overlaps(tmp, i) OR next(tmp) =  first(i) THEN
				tmp := period_union(tmp, i);
			ELSE
				ret := array_append(ret, tmp);
				tmp:=i;
			END IF;
		END IF;
	END LOOP;
	IF tmp IS NOT NULL THEN
		ret := array_append(ret, tmp);
	END IF;
	RETURN ret;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Checks if one list of periods is fully covered by second list
CREATE OR REPLACE FUNCTION periods_covered_by(referencing PERIOD[], referenced PERIOD[]) RETURNS boolean AS
$BODY$
DECLARE
	found BOOLEAN;
	i PERIOD;
	j PERIOD;
BEGIN
	FOREACH i IN ARRAY referencing LOOP
		found := false;
		FOREACH j IN ARRAY referenced LOOP
			IF contained_by(i, j) THEN
				found := true;
			END IF;
		END LOOP;
		IF NOT found THEN
			RETURN false;
		END IF;
	END LOOP;
	RETURN true;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Joining 2 sets of columns for join expressions
CREATE OR REPLACE FUNCTION JoinColList(cols1 TEXT[], tabname1 TEXT,cols2 TEXT[], tabname2 TEXT) RETURNS TEXT AS
$BODY$
DECLARE
	ret TEXT := '';
	t TEXT;
	j INTEGER := 0;
BEGIN
	FOR i IN 1 .. array_upper(cols1, 1) LOOP
	IF j > 0 THEN
		ret := ret || ' AND ';
	END IF;
	j := j + 1;
	ret := ret || tabname1 || '.' || cols1[i] || '=' || tabname2 || '.' || cols2[i];
	END LOOP;
	return ret;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Simple function to join list of columns with commas inbetween
CREATE OR REPLACE FUNCTION simpleColList(cols TEXT[], tabname TEXT) RETURNS TEXT AS
$BODY$
DECLARE ret TEXT := '';
	t TEXT;
	i INTEGER := 0;
BEGIN
	
	FOREACH t IN ARRAY cols LOOP
	IF i > 0 THEN
		ret := ret || ' , ';
	END IF;
	i := i + 1;
	ret := ret || tabname || '.' || t;
	END LOOP;
	return ret;
END;
$BODY$ LANGUAGE 'plpgsql';
-- @End helper functions


-- @Start Transaction Time tables support functions

-- Trigger function that moves the original line to historical table before doing updates
-- This function also sets correct transaction time for new line
CREATE OR REPLACE FUNCTION MoveExistingToHistory() RETURNS trigger AS
$BODY$
BEGIN
	OLD.transaction_time := period(first(OLD.transaction_time), now());
	NEW.transaction_time := period(now(), 'infinity'::timestamp);
	EXECUTE FORMAT('INSERT INTO %ITransactionHistory SELECT $1.*', TG_TABLE_NAME) USING OLD;
	RETURN NEW;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Trigger function handling new line insertions for transaction time table
-- This function only sets transaction time to its current value
CREATE OR REPLACE FUNCTION AddTransactionTimeToLine() RETURNS trigger AS
$BODY$
BEGIN
	NEW.transaction_time := period(now(), 'infinity'::timestamp);
	RETURN NEW;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Trigger function to move line to historical table in case it is being deleted
CREATE OR REPLACE FUNCTION MoveLineToHistory() RETURNS trigger AS
$BODY$
BEGIN
	OLD.transaction_time := period(first(OLD.transaction_time), now());
	EXECUTE FORMAT('INSERT INTO %ITransactionHistory SELECT $1.*', TG_TABLE_NAME) USING OLD;
	RETURN OLD;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Trigger function to prevent historical data manipulation
CREATE OR REPLACE FUNCTION PreventHistoricalDataManipulation() RETURNS trigger AS
$BODY$
BEGIN
	RAISE EXCEPTION 'Attempt at changing historical data.';
END;
$BODY$ LANGUAGE 'plpgsql';

-- @End Transaction Time tables support functions

-- Main function to turn regular table into a transaction time one
CREATE OR REPLACE FUNCTION addTransactionTime(t_table TEXT) RETURNS void AS $$
BEGIN
	-- Adding transaction time column
	EXECUTE FORMAT('ALTER TABLE %s ADD COLUMN transaction_time PERIOD', t_table);
	-- Creating historical table
	EXECUTE FORMAT('CREATE TABLE %sTransactionHistory (LIKE %s INCLUDING ALL)', t_table, t_table);
	-- Creating view to see present+history at the same time
	EXECUTE FORMAT('CREATE OR REPLACE VIEW %sHistorical AS SELECT * FROM %s UNION SELECT * FROM %sTransactionHistory', t_table, t_table, t_table);
	-- Adding triggers to handle transaction time population
	EXECUTE FORMAT('CREATE TRIGGER %sUpdate BEFORE UPDATE ON %s  FOR EACH ROW EXECUTE PROCEDURE MoveExistingToHistory();', t_table, t_table);
	EXECUTE FORMAT('CREATE TRIGGER %sInsert BEFORE INSERT ON %s  FOR EACH ROW EXECUTE PROCEDURE AddTransactionTimeToLine();', t_table, t_table, t_table);
	EXECUTE FORMAT('CREATE OR REPLACE VIEW %sHistorical AS SELECT * FROM %s UNION SELECT * FROM %sTransactionHistory', t_table, t_table, t_table);
	-- Preventing actions on the historical table
	EXECUTE FORMAT('CREATE TRIGGER %sPreventChanges BEFORE UPDATE OR DELETE ON %sTransactionHistory 
			EXECUTE PROCEDURE PreventHistoricalDataManipulation()', t_table, t_table);
END;	
$$ LANGUAGE plpgsql;

-- @Start Support structures and functions for valid time tables

-- Table to hold original primary keys for tables being turned valid time
DROP TABLE IF EXISTS validTimePrimaryKeys;
CREATE TABLE validTimePrimaryKeys (
		table_name TEXT,
		-- List of columns that original primary key consisted of
		key_cols TEXT[]
		);

-- Helper Function creating primary key id mappings
CREATE OR REPLACE FUNCTION getMappingString(cols TEXT[], tabname TEXT) RETURNS TEXT AS
$BODY$
DECLARE ret TEXT := '';
	t TEXT;
	i INTEGER := 0;
BEGIN
	
	FOREACH t IN ARRAY cols LOOP
	IF i > 0 THEN
		ret := ret || ' AND ';
	END IF;
	i := i + 1;
	ret := ret || tabname || '.' || t || '=$1.' || t;
	END LOOP;
	return ret;
END;
$BODY$ LANGUAGE 'plpgsql';		

-- Simple function to check if any any of given intervals overlaps with interval passed as second parameter
CREATE OR REPLACE FUNCTION DoIntervalsOverlap(intervals Period[], newinterval PERIOD) RETURNS BOOLEAN AS
$BODY$
DECLARE
	int1 PERIOD;
	count INT := 0;
BEGIN
	IF array_length(intervals, 1) < 1 THEN
		RETURN FALSE;
	END IF;
	FOREACH int1 IN ARRAY intervals LOOP
			IF overlaps(int1, newinterval) THEN
				count := count + 1;
			END IF;
	END LOOP;
	RETURN count > 1;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Main valid_time related function. If inserting new fact about existing entity, this splits
-- and shortens existing intervals accordingly
CREATE OR REPLACE FUNCTION AdjustOriginalValidTime() RETURNS TRIGGER AS
$BODY$
DECLARE
	LineToSplit record;
	tmp_period period;
	pk_cols TEXT[];
	pk_cond TEXT;
BEGIN

	-- Retrieve primary key condition
	EXECUTE 'SELECT key_cols FROM ValidTimePrimaryKeys WHERE lower(table_name) = $1' INTO pk_cols USING TG_TABLE_NAME;
	IF pk_cols IS NULL THEN
		RETURN NEW;
	END IF;
	EXECUTE 'SELECT getMappingString($1, $2)' INTO pk_cond USING pk_cols, TG_TABLE_NAME;
	-- First delete all lines completely comsumed
	EXECUTE FORMAT('DELETE FROM %I WHERE %s AND contained_by(%I.valid_time, $1.valid_time)', TG_TABLE_NAME, pk_cond,  TG_TABLE_NAME) USING NEW;

	-- Now update all lines that intersect but do not contain new interval
	EXECUTE FORMAT('UPDATE %I SET valid_time = minus(%I.valid_time, $1.valid_time) WHERE %s AND 
			(%I.valid_time &< $1.valid_time OR %I.valid_time &> $1.valid_time)', TG_TABLE_NAME, TG_TABLE_NAME, pk_cond, TG_TABLE_NAME, TG_TABLE_NAME) USING NEW;

	-- Lastly we split lines containing the new interval into two.
	EXECUTE FORMAT('SELECT * FROM %I WHERE %s AND contained_by($1.valid_time, %I.valid_time)', TG_TABLE_NAME, pk_cond,  TG_TABLE_NAME) INTO LineToSplit USING NEW;
	IF LineToSplit IS NOT NULL THEN
		tmp_period := LineToSplit.valid_time;
		EXECUTE FORMAT('DELETE FROM %I WHERE %s AND contained_by($1.valid_time, %I.valid_time)', TG_TABLE_NAME, pk_cond,  TG_TABLE_NAME) USING NEW;
		LineToSplit.valid_time = period(first(tmp_period), first(NEW.valid_time));
		EXECUTE 'INSERT INTO ' || TG_TABLE_NAME ||' SELECT (' || quote_literal(LineToSplit) || '::' || TG_RELID::regclass || ').*';
		LineToSplit.valid_time = period(next(NEW.valid_time), next(tmp_period));
		EXECUTE 'INSERT INTO ' || TG_TABLE_NAME ||' SELECT (' || quote_literal(LineToSplit) || '::' || TG_RELID::regclass || ').*';
	END IF;

	return NEW;
	
END;
$BODY$ LANGUAGE 'plpgsql';

-- Function that verifies that any inserted line does not 
CREATE OR REPLACE FUNCTION KeepValidTimeInSync() RETURNS TRIGGER AS
$BODY$
DECLARE
	intervals PERIOD[]; 
	interval PERIOD;
	pk_cols TEXT[];
	pk_cond TEXT;
BEGIN
	-- Retrieve primary key condition
	EXECUTE 'SELECT key_cols FROM ValidTimePrimaryKeys WHERE lower(table_name) = $1' INTO pk_cols USING TG_TABLE_NAME;
	EXECUTE 'SELECT getMappingString($1, $2)' INTO pk_cond USING pk_cols, TG_TABLE_NAME;

	-- Select all existing intervals and check for overlaps
	EXECUTE FORMAT ('SELECT array_agg(valid_time) FROM %I WHERE %s', TG_TABLE_NAME, pk_cond) INTO intervals USING NEW;	
	if DoIntervalsOverlap(intervals, NEW.valid_time) THEN
		RAISE EXCEPTION 'INVALID VALID TIME';
	END IF;
	RETURN NEW;
END;
$BODY$ LANGUAGE 'plpgsql';
-- @End Support structures and functions for valid time tables

-- Main function to convert table into a valid time table
CREATE OR REPLACE FUNCTION addValidTime(t_table TEXT) RETURNS VOID AS
$BODY$
DECLARE
	pk_name TEXT;
BEGIN
	-- Save primary keys, as they are necessary for keeping track of what an entity is
	EXECUTE 'INSERT INTO ValidTimePrimaryKeys
		 SELECT '''||t_table||''', array_agg(a.attname) AS data_type
		 FROM   pg_index i
		 JOIN   pg_attribute a ON a.attrelid = i.indrelid
                       AND a.attnum = ANY(i.indkey)
		 WHERE  i.indrelid = '''||t_table||'''::regclass
		 AND    i.indisprimary';
		 
	-- Selecting PK constraint name to be able to drop it
	EXECUTE 'SELECT conname FROM pg_constraint WHERE conrelid  = '''||t_table||'''::regclass AND contype = ''p''' INTO pk_name;
	IF pk_name IS NOT NULL THEN
		EXECUTE 'ALTER TABLE '||t_table||' DROP CONSTRAINT '||pk_name||' CASCADE';
	END IF;
	-- Adding valid time trigger
	EXECUTE FORMAT('ALTER TABLE %s ADD COLUMN valid_time PERIOD', t_table);
	
	-- Adding trigger to adjust valid time intervals when adding new facts 
	EXECUTE FORMAT('CREATE TRIGGER %s1KeepValidTimeReferences AFTER INSERT ON %s FOR EACH ROW EXECUTE PROCEDURE checkTableReferences()', t_table, t_table);
	EXECUTE FORMAT('CREATE TRIGGER %s2AdjustIntervalsForInsertion BEFORE INSERT ON %s FOR EACH ROW EXECUTE PROCEDURE AdjustOriginalValidTime()', t_table, t_table);
	EXECUTE FORMAT('CREATE TRIGGER %s3ValidConsistency AFTER UPDATE ON %s FOR EACH ROW EXECUTE PROCEDURE KeepValidTimeInSync()', t_table, t_table);
END;
$BODY$ LANGUAGE 'plpgsql';

-- @Start Functions for valid time reference integrity manipulations
-- Trigger function that raises an exception if there is a corrupted reference
CREATE OR REPLACE FUNCTION checkTableReferences() RETURNS TRIGGER AS
$BODY$
DECLARE
	referenced_name TEXT; 
	referencing_table TEXT; 
	f_keys TEXT[];
	refing_cols TEXT;
	refed_cols TEXT;
	p_keys TEXT[];
	cond TEXT;
	ret boolean;
	a period[];
	b period[];
BEGIN
	IF pg_trigger_depth() > 1 THEN 
		RETURN NEW;
	END IF;
	-- Iterates over all relationships where altered table is the referencing side
	FOR f_keys, referencing_table IN EXECUTE 'SELECT ForeignKeys, ReferencingTable FROM ValidTimeReferences WHERE lower(ReferencedTable) = lower($1)' USING TG_TABLE_NAME LOOP
		referenced_name := TG_TABLE_NAME;
		-- Finds primary key of referenced table
		EXECUTE 'SELECT key_cols FROM validTimePrimaryKeys WHERE lower(table_name) = lower($1)' INTO p_keys USING referenced_name;	
		-- Converts foreign key into SQL fragment
		EXECUTE 'SELECT simpleColList($1, $2)' INTO refing_cols USING f_keys, referencing_table;
		-- Converts primary key into SQL fragment
		EXECUTE 'SELECT simpleColList($1, $2)' INTO refed_cols USING p_keys, referenced_name;
		-- Checks if there are any mismatched periods
		EXECUTE 'SELECT periods_covered_by(refing.vt, refed.vt) FROM 
			(SELECT '||refing_cols||', restructuralize_periods(array_agg(valid_time)) vt 
			FROM '||referencing_table||' 
			GROUP BY '||refing_cols||') refing
			JOIN 
			(SELECT '||refed_cols||', restructuralize_periods(array_agg(valid_time)) vt 
			FROM '||referenced_name||' 
			WHERE '||getMappingString(p_keys, referenced_name)||'
			GROUP BY '||refed_cols||') refed
			ON '||JoinColList(f_keys, 'refing', p_keys, 'refed') INTO ret USING NEW;
		IF NOT ret THEN
			RAISE EXCEPTION 'Corrupted refferencial integrity for tables %->% attempting to change referenced entity', referencing_table, referenced_name;
		END IF;
	END LOOP;
	-- Iterates over all relationships where altered table is the referenced side, rest is similar to previous loop
	FOR f_keys, referenced_name IN EXECUTE 'SELECT ForeignKeys, ReferencedTable FROM ValidTimeReferences WHERE lower(ReferencingTable) = lower($1)' USING TG_TABLE_NAME LOOP
		referencing_table := TG_TABLE_NAME;
		EXECUTE 'SELECT key_cols FROM validTimePrimaryKeys WHERE lower(table_name) = lower($1)' INTO p_keys USING referenced_name;	
		EXECUTE 'SELECT simpleColList($1, $2)' INTO refing_cols USING f_keys, referencing_table;
		EXECUTE 'SELECT simpleColList($1, $2)' INTO refed_cols USING p_keys, referenced_name;
		EXECUTE 'SELECT periods_covered_by(refing.vt, refed.vt) FROM 
			(SELECT '||refing_cols||', restructuralize_periods(array_agg(valid_time)) vt 
			FROM '||referencing_table||' 
			WHERE '||getMappingString(f_keys, referencing_table)||'
			GROUP BY '||refing_cols||') refing
			JOIN 
			(SELECT '||refed_cols||', restructuralize_periods(array_agg(valid_time)) vt 
			FROM '||referenced_name||' 
			GROUP BY '||refed_cols||') refed
			ON '||JoinColList(f_keys, 'refing', p_keys, 'refed') INTO ret USING NEW;
		IF NOT ret THEN
			RAISE EXCEPTION 'Corrupted refferencial integrity for table %->% trying to change referencing entity', referencing_table, referenced_name;
		END IF;
	END LOOP;
	RETURN NEW;
	
END;
$BODY$ LANGUAGE 'plpgsql';

-- This table contains all valid time foreign keys present in the system
DROP TABLE IF EXISTS validTimeReferences;
CREATE TABLE ValidTimeReferences (
	ReferencingTable TEXT,
	ReferencedTable TEXT,
	ForeignKeys TEXT[]
);

-- Simple function to add a valid time foreign key constraint
CREATE OR REPLACE FUNCTION addValidTimeReference(referencingTable TEXT, referencedTable TEXT, foreignKeys TEXT[]) RETURNS VOID AS
$BODY$
BEGIN
	EXECUTE 'INSERT INTO ValidTimeReferences VALUES ($1, $2, $3)' USING referencingTable, referencedTable, foreignKeys;
END;
$BODY$ LANGUAGE 'plpgsql';
-- @End Functions for valid time reference integrity manipulations


-- @Begin Additional aggregate functions
-- Function returning period with lower start dated
CREATE OR REPLACE FUNCTION lower_start(p1 PERIOD, p2 PERIOD) RETURNS PERIOD AS
$BODY$
BEGIN
	IF p1 is NULL THEN RETURN p2; END IF;
	IF p2 is NULL THEN RETURN p1; END IF;
	IF prior(p2) < prior(p1) THEN RETURN p2; END IF;
	IF prior(p2) > prior(p1) THEN RETURN p1; END IF;
	IF last(p2) < last(p1) THEN RETURN p2; END IF;
	RETURN p1;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Aggregate function returning the period with lowest start date
DROP AGGREGATE IF EXISTS min_start(PERIOD);
CREATE AGGREGATE min_start(PERIOD)(
    sfunc = lower_start,
    stype = PERIOD);

-- Function returning period with higher start date
CREATE OR REPLACE FUNCTION higher_start(p1 PERIOD, p2 PERIOD) RETURNS PERIOD AS
$BODY$
BEGIN
	IF p1 is NULL THEN RETURN p2; END IF;
	IF p2 is NULL THEN RETURN p1; END IF;
	IF prior(p2) < prior(p1) THEN RETURN p1; END IF;
	IF prior(p2) > prior(p1) THEN RETURN p2; END IF;
	IF last(p2) > last(p1) THEN RETURN p2; END IF;
	RETURN p1;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Aggregate function returning the period with highest start date
DROP AGGREGATE IF EXISTS max_start(PERIOD);
CREATE AGGREGATE max_start(PERIOD)(
    sfunc = higher_start,
    stype = PERIOD);

-- Function returning period with lower end date
CREATE OR REPLACE FUNCTION lower_end(p1 PERIOD, p2 PERIOD) RETURNS PERIOD AS
$BODY$
BEGIN
	IF p1 is NULL THEN RETURN p2; END IF;
	IF p2 is NULL THEN RETURN p1; END IF;
	IF last(p2) < last(p1) THEN RETURN p2; END IF;
	IF last(p2) > last(p1) THEN RETURN p1; END IF;
	IF prior(p2) < prior(p1) THEN RETURN p2; END IF;
	RETURN p1;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Aggregate function returning the period with lowest end date
DROP AGGREGATE IF EXISTS min_end(PERIOD);
CREATE AGGREGATE min_end(PERIOD)(
    sfunc = lower_end,
    stype = PERIOD);

-- Function returning period with higher end date
CREATE OR REPLACE FUNCTION higher_end(p1 PERIOD, p2 PERIOD) RETURNS PERIOD AS
$BODY$
BEGIN
	IF p1 is NULL THEN RETURN p2; END IF;
	IF p2 is NULL THEN RETURN p1; END IF;
	IF last(p2) < last(p1) THEN RETURN p1; END IF;
	IF last(p2) > last(p1) THEN RETURN p2; END IF;
	IF prior(p2) > prior(p1) THEN RETURN p2; END IF;
	RETURN p1;
END;
$BODY$ LANGUAGE 'plpgsql';

-- Aggregate function returning the period with highest end date
DROP AGGREGATE IF EXISTS max_end(PERIOD);
CREATE AGGREGATE max_end(PERIOD)(
    sfunc = higher_end,
    stype = PERIOD);

-- Aggregate function to support valid time sequencing operation
DROP AGGREGATE IF EXISTS sequenced(PERIOD);
CREATE AGGREGATE sequenced (PERIOD)(
    sfunc = array_append,
    stype = PERIOD[],
    initcond = '{}',
    finalfunc = restructuralize_periods);

-- Function returning an 'envelope' for given two periods
-- Envelope is smalles period containing both input periods
CREATE OR REPLACE FUNCTION env_period(p1 PERIOD, p2 PERIOD) RETURNS PERIOD AS
$BODY$
DECLARE
	st TIMESTAMP;
	en TIMESTAMP;
BEGIN
	IF p1 is NULL THEN RETURN p2; END IF;
	IF p2 is NULL THEN RETURN p1; END IF;
	IF last(p2) < last(p1) THEN en = last(p1); ELSE en = last(p2); END IF;
	IF first(p2) > first(p1) THEN st = first(p1); ELSE st = first(p1); END IF;
	RETURN PERIOD(st, en);
END;
$BODY$ language 'plpgsql';

-- Aggregate function returning 'envelope' of all given periods
DROP AGGREGATE IF EXISTS envelope(PERIOD);
CREATE AGGREGATE envelope(PERIOD)(
	sFunc = env_period,
	stype = PERIOD);

-- Aggregate function returning intersection of all given periods
DROP AGGREGATE IF EXISTS p_intersect(PERIOD);
CREATE AGGREGATE p_intersect(PERIOD)(
	sFunc = period_intersect,
	sType = PERIOD);
-- @end Additional aggregate functions




